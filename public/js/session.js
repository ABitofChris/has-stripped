/*
 *  Copyright 2016 Google Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License")
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

"use strict";

/**
 * Module for auth and session management
 */
var Session = (function() {
    var loginDialog;
    var createDialog;
    var loggedIn = false;

    /*
     * Firebase handler for when authentication state
     * changes
     * */
    function authStateChangeListener(user) {
       
    }

    /*
     * Sign in with a username and password
     * */
    function signInWithEmailandPassword() {
     
    }

    /*
     * Sign in with you Google Account
     * */
    function signInWithGoogle() {
       
    }

    /*
     * Close the login dialog, but only if it's open
     * */
    function closeLoginDialog() {
    
    }

    /*
     * Do the work to actually create an account
     * with Firebase
     * */
    function submitCreateAccount() {
   
    }

    // Exported functions
    return {
        /*
         * Sets up event listeners for login/logout UI elements.
         * */
        init: function() {
            loginDialog = document.querySelector("#login-dialog");

            firebase.auth().onAuthStateChanged(authStateChangeListener);

            //login/logout
            document.querySelector("#login").addEventListener("click", function() {
                loginDialog.showModal();
            });
            document.querySelector("#logout").addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    console.log('Signed Out');
                }, function(error) {
                    console.error('Sign Out Error', error);
                });
            });
            document.querySelector("#sign-in").addEventListener("click", signInWithEmailandPassword);

            //google login
            document.querySelector("#google-signin img").addEventListener("click", signInWithGoogle);

            //create accounts
            createDialog = document.querySelector("#create-account-dialog");
            document.querySelector("#create-account").addEventListener("click", function() {
                createDialog.showModal();
            });
            document.querySelector("#entry-submit").addEventListener("click", submitCreateAccount);
        },
    }
})();